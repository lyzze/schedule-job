package com.ztjz.schedulejob.task;

import lombok.extern.slf4j.Slf4j;

/**
 * @Author: liuyuze
 * @Date: 2020.9.4 15:30
 */
@Slf4j
public class TaskClazz {

    public void ryNoParams()
    {
        log.info("执行TaskClazz无参方法");
    }
}
